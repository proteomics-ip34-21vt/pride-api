# Pride API Interface

[![pipeline status](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/pride-api/badges/master/pipeline.svg)](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/pride-api/-/commits/master)
[![coverage report](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/pride-api/badges/master/coverage.svg)](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/pride-api/-/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)

Usage:

```python
from typing import Iterable
from pride_api.endpoint import files, projects
from pride_api.prideObjects import PrideProject, PrideFile

request = projects.project_search(("human", ))
project_data: List[PrideProject] = next(request)

for project in project_data[:2]:
    accession: str = project.accession
    print(f"\nProject: {accession}")
    print(f"=" * (9 + len(accession)))

    prj_files: List[PrideFile] = next(files.get_files_by_project(accession))

    for a_file in prj_files:
        print(a_file.file_name)
```
