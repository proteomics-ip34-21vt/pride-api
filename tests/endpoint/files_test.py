"""Tests the 'file' PRIDE endpoint class."""

from pride_api.endpoint import files
from pride_api.parsers.file import FileByProjectParser


def test_get_files_by_project() -> None:
    """Tests if the 'files by project' helper maps the correct values to the request."""
    request = files.get_files_by_project(accession="ABCDEF")
    assert request.endpoint == "files/byProject"
    assert request.parameters == {"accession": "ABCDEF"}
    assert request.pagination is None
    assert request.parser == FileByProjectParser
