"""Tests the 'project' PRIDE endpoint class."""

from pride_api.defaults import PrideOrdering
from pride_api.endpoint import projects
from pride_api.pagination import Pagination
from pride_api.parsers.project import PrideProjectSearchParser, PrideProjectsParser


def test_all_projects() -> None:
    """Tests if the 'all projects' function maps the correct values to the request."""
    pager = Pagination(page_size=5)
    request = projects.all_projects(pagination=pager)
    assert request.endpoint == "projects"
    assert request.parameters == {}
    assert request.pagination == pager
    assert request.parser == PrideProjectsParser


def test_project_search() -> None:
    """Tests if the 'project search' function maps the correct values to the request."""
    pager = Pagination(page_size=5)
    request = projects.project_search(
        keywords=("k1", "K2"),
        filters={"f1": "v1", "f2": "v2 9"},
        date_gap="+1MONTH",
        sort_direction=PrideOrdering.DESCENDING,
        sort_fields=("uploadDate",),
        pagination=pager,
    )
    assert request.endpoint == "search/projects"
    assert request.parameters == {
        "keyword": "k1,K2",
        "filter": "f1==v1,f2==v2 9",
        "dateGap": "+1MONTH",
        "sortDirection": "DESC",
        "sortFields": "uploadDate",
    }
    assert request.pagination == pager
    assert request.parser == PrideProjectSearchParser


def test_project_accession() -> None:
    """Tests if the 'project accession' function maps the correct values to the request."""
    request = projects.project_accession("PXD12345678")
    assert request.endpoint == "projects/PXD12345678"
    assert request.parameters == {}
    assert request.pagination is None
    assert request.parser == PrideProjectsParser
