# pylint: disable=duplicate-code
"""Tests the pride file API parser implementation."""

import json
from datetime import datetime
from typing import Any, List, Sequence, cast

import pytest

from pride_api.defaults import DEFAULT_FILE_ENCODING
from pride_api.parsers.file import FileByProjectParser
from pride_api.pride_objects import PrideFile, PrideLink, PrideParam


@pytest.fixture(name="files_by_project_data")
def files_by_project_data_fixture() -> List[Any]:
    """Pride files by project data test fixture. Returns a cached list of pride project files simulating the API."""
    with open("./tests/api/files-byProject.json", encoding=DEFAULT_FILE_ENCODING) as fh:
        cont = fh.read()

    return cast(List[Any], json.loads(cont))


def test_project_parse(files_by_project_data: Sequence[Any]) -> None:
    """Test the project parser."""
    parsed = FileByProjectParser.parse(files_by_project_data)

    assert len(parsed) == 30
    assert parsed[0] == PrideFile(
        project_accessions=("PXD005011",),
        accession="101877abd2d37c950afb8f4d9f97a1d79821193c2204fefc2e3c414377c5308f",
        file_category=PrideParam(
            param_type="CvParam",
            cv_label="PRIDE",
            accession="PRIDE:0000404",
            name="Associated raw file URI",
            value="RAW",
        ),
        checksum="7de555bc4f6fc0c2635e22981de31935ab0c95df",
        public_file_locations=(
            PrideParam(
                param_type="CvParam",
                cv_label="PRIDE",
                accession="PRIDE:0000468",
                name="Aspera Protocol",
                value="prd_ascp@fasp.ebi.ac.uk:pride/data/archive/2018/04/PXD005011/OTNCS25cm_GG01_C1-4.raw",
            ),
            PrideParam(
                param_type="CvParam",
                cv_label="PRIDE",
                accession="PRIDE:0000469",
                name="FTP Protocol",
                value="ftp://ftp.ebi.ac.uk/pride-archive/2018/04/PXD005011/OTNCS25cm_GG01_C1-4.raw",
            ),
        ),
        file_size_bytes=469601052,
        file_name="OTNCS25cm_GG01_C1-4.raw",
        compress=False,
        submission_date=datetime(
            year=2016, month=9, day=21, hour=11, minute=57, second=6
        ),
        publication_date=datetime(
            year=2018, month=4, day=30, hour=14, minute=26, second=28
        ),
        updated_date=datetime(
            year=2016, month=9, day=21, hour=11, minute=59, second=28
        ),
        additional_attributes=tuple(),
        links=(
            PrideLink(
                rel="self",
                href="https://www.ebi.ac.uk/pride/ws/archive/v2/files/101877abd2d37c950afb8f4d9f97a1d79821193c2204fefc2e3c414377c5308f",
            ),
        ),
    )


def test_parse_non_list() -> None:
    """Test if the file by project parser throws exceptions if we don't have a list."""
    with pytest.raises(ValueError):
        FileByProjectParser.parse({})

    with pytest.raises(ValueError):
        FileByProjectParser.parse(tuple())
