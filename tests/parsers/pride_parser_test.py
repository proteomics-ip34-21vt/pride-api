"""Pride parser tests."""
# pylint: disable=duplicate-code,protected-access

from pride_api.parsers.pride_parser import PrideParser
from pride_api.pride_objects import PrideLink, PrideParam, PridePerson


def test_pride_date_format() -> None:
    """Test if we are using the correct pride date format."""
    assert PrideParser.PRIDE_DATE_FORMAT == "%Y-%m-%d"


def test_parse_pride_file_param() -> None:
    """Test if we handle the pride file param info processing correctly."""
    params = PrideParser._parse_pride_file_param(
        [
            {
                "@type": "@type1",
                "cvLabel": "cvLabel1",
                "accession": "accession1",
                "name": "name1",
                "value": "value1",
            },
            {
                "@type": "@type2",
                "cvLabel": "cvLabel2",
                "accession": "accession2",
                "name": "name2",
            },
        ]
    )

    assert params == (
        PrideParam(
            param_type="@type1",
            cv_label="cvLabel1",
            accession="accession1",
            name="name1",
            value="value1",
        ),
        PrideParam(
            param_type="@type2",
            cv_label="cvLabel2",
            accession="accession2",
            name="name2",
            value=None,
        ),
    )


def test_parse_pride_person() -> None:
    """Test if we handle the pride person info processing correctly."""
    people = PrideParser._parse_pride_person(
        [
            {
                "title": "title1",
                "affiliation": "affiliation1",
                "email": "email1",
                "country": "country1",
                "orcid": "orcid1",
                "name": "name1",
                "id": "id1",
            },
            {
                "title": "title2",
                "affiliation": "affiliation2",
                "email": "email2",
                "country": "country2",
                "orcid": "orcid2",
                "name": "name2",
                "id": "id2",
            },
        ]
    )

    assert people == (
        PridePerson(
            title="title1",
            affiliation="affiliation1",
            email="email1",
            country="country1",
            orcid="orcid1",
            name="name1",
            id="id1",
        ),
        PridePerson(
            title="title2",
            affiliation="affiliation2",
            email="email2",
            country="country2",
            orcid="orcid2",
            name="name2",
            id="id2",
        ),
    )


def test_parse_pride_links() -> None:
    """Test if we handle the pride link info processing correctly."""
    links = PrideParser._parse_pride_links(
        [
            {
                "rel": "rel1",
                "href": "href1",
            },
            {
                "rel": "rel2",
                "href": "href2",
            },
        ]
    )

    assert links == (
        PrideLink(rel="rel1", href="href1"),
        PrideLink(rel="rel2", href="href2"),
    )
