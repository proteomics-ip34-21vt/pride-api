"""Tests the pride project API parser implementation."""

import json
from datetime import datetime
from typing import Any, Dict, cast

import pytest

from pride_api.defaults import DEFAULT_FILE_ENCODING
from pride_api.parsers.project import PrideProjectSearchParser, PrideProjectsParser
from pride_api.pride_objects import PrideLink, PrideParam, PridePerson, PrideProject
from pride_api.pride_objects import PrideProjectSearchResult


@pytest.fixture(name="projects_data")
def projects_data_fixture() -> Dict[str, Any]:
    """Pride project data test fixture. Returns a cached list of pride projects simulating the API."""
    with open("./tests/api/search-projects.json", encoding=DEFAULT_FILE_ENCODING) as fh:
        cont = fh.read()

    return cast(Dict[str, Any], json.loads(cont)["_embedded"])


@pytest.fixture(name="project_data")
def project_data_fixture() -> Dict[str, Any]:
    """Pride project data test fixture. Returns a cached pride project simulating the API."""
    with open("./tests/api/single-project.json", encoding=DEFAULT_FILE_ENCODING) as fh:
        cont = fh.read()

    return cast(Dict[str, Any], json.loads(cont))


@pytest.fixture(name="project_data_parsed")
def project_data_parsed_fixture() -> PrideProject:
    """Pride project data test fixture. Returns a cached, parsed pride project simulating the API."""
    return PrideProject(
        accession="PXD033340",
        title="Multi-Omics and Informatics Analysis of FFPE Tissues Derived from Melanoma Patients "
        + "with Long/Short Responses to Anti-PD1 Therapy Reveals Pathways of Response",
        additional_attributes=(
            PrideParam(
                param_type="CvParam",
                cv_label="PRIDE",
                accession="PRIDE:0000411",
                name="Dataset FTP location",
                value="ftp://ftp.pride.ebi.ac.uk/pride/data/archive/2022/04/PXD033340",
            ),
        ),
        project_description="Anti-PD-1 based immune therapies are thought to be dependent "
        + "on antigen processing and presentation mechanisms. To characterize the immune-dependent "
        + "mechanisms that predispose stage III/IV melanoma patients to respond to anti-PD-1 "
        + "therapies, we performed a multi-omics study consisting of expression proteomics "
        + "and targeted immune-oncology-based mRNA sequencing. Formalin-fixed paraffin-embedded "
        + "tissue samples were obtained from stage III/IV patients with melanoma prior to "
        + "anti-PD-1 therapy. The patients were first stratified into poor and good responders "
        + "based on whether their tumors had or had not progressed while on anti-PD-1 therapy "
        + "for 1 year. We identified 263 protein/gene candidates that displayed differential "
        + "expression, of which 223 were identified via proteomics and 40 via targeted-mRNA "
        + "analyses. The downstream analyses of expression profiles using MetaCore software "
        + "demonstrated an enrichment of immune system pathways involved in antigen processing/"
        + "presentation and cytokine production/signaling. Pathway analyses showed interferon "
        + "(IFN)-γ-mediated signaling via NF-κB and JAK/STAT pathways to affect immune processes "
        + "in a cell-specific manner and to interact with the inducible nitric oxide synthase. "
        + "We review these findings within the context of available literature on the efficacy "
        + "of anti-PD-1 therapy. The comparison of good and poor responders, using efficacy of "
        + "PD-1-based therapy at 1 year, elucidated the role of antigen presentation in mediating "
        + "response or resistance to anti-PD-1 blockade.",
        sample_processing_protocol="To overcome the dynamic range in protein amounts from abundant, "
        + "such as actin, to minute, proteomic workflows, we typically used peptide prefractionation "
        + "and liquid chromatography–mass spectrometry (i.e., LC–MS/MS). Even with multiplexing, "
        + "instrument time and cost remain barriers to the analysis of large cohorts [118]. "
        + "Furthermore, some clinical samples (such as tissue microarray or laser capture "
        + "microdissected specimens) have limited quantities of total protein (i.e., 1–2 μg) and "
        + "are too small for effective prefractionation, such as the ones in this study. Therefore, "
        + "digests of human FFPE melanoma tissue samples were analyzed using LC–MS/MS without "
        + "prefractionation, but an alternative strategy can be used to generate additional "
        + "protein identification and quantification. A pooled sample was created from excess "
        + "material from each tryptic digest, fractionated with basic pH reversed-phase liquid "
        + "chromatography, and analyzed with LC–MS/MS. Then, the data analysis strategy includes "
        + "peptide sequence identification from the individual LC–MS/MS data generated from each "
        + "patient sample and matched to a library generated from the multiple LC–MS/MS analyses "
        + "of the fractionated peptides from the pooled sample, which contained an equal mixture "
        + "of all the samples in the patient cohort (Figure 1). Relative quantitation was "
        + "significantly enhanced by matching the data to a comprehensive peptide library "
        + "generated from the analysis of the pool in the same batch of samples. This matching "
        + "greatly increased the number of peptide identifications, thereby providing more evidence "
        + "for the measurement of each protein. FFPE tissue slices were deparaffinized with xylene, "
        + "then rehydrated sequentially in 100%, 85%, and 70% ethanol. To assist in antigen "
        + "retrieval, incubations with aqueous ammonium bicarbonate were performed for 2 h at 80 °C"
        + " and 1 h at 60 °C, followed by sonication. After measuring the protein concentration, the"
        + " proteins were reduced, alkylated, and digested with trypsin (enzyme-to-substrate w/w "
        + "ratio = 1:20), and the peptides were desalted using SepPak C18 cartridges (Hypersep C18, "
        + "ThermoFisher, Waltham, MA, USA). A pooled peptide library was generated from an LC–MS/MS "
        + "analysis of 24 fractions concatenated after basic pH reversed-phase liquid chromatography. "
        + "A Dionex U3000 nanoUPLC coupled to a Q Exactive Plus mass spectrometer (ThermoFisher "
        + "Waltham, MA, USA) was used to analyze the fractions and each individual FFPE tissue sample.",
        data_processing_protocol="The software packages Mascot and Sequest contained in Proteome "
        + "Discoverer (Thermo) were used to identify the proteins, and MaxQuant analyses were "
        + "performed for relative quantification.  This upload contains the Mascot database search "
        + "results. The data generated in the mRNA and proteomic experiments were analyzed. Within "
        + "a sample derived from a patient, the mRNA panel was normalized by dividing the abundance "
        + "of each mRNA by the geometric mean of 14 (out of 15) housekeeping genes. One housekeeping "
        + "gene, TBP, was excluded from the analysis due to low signal intensity. A scaling factor of "
        + "400,000 was applied because it was the lowest multiple of 100,000 to result in no log2 "
        + "values <1. Protein spectra, quantified using MaxQuant (Max Planck Institute, Martinsried, "
        + "Germany), were normalized using IRON (Iterative Rank-Order Normalization) (iron_generic "
        + "--proteomics) against the median sample (findmedian --spreadsheet --pearson). Downstream "
        + "2-group differential expression analyses were performed (good and poor responders) and "
        + "filtered using the following cutoffs: log2 ratio ≥log2 (1.5-fold change), t test <0.05, "
        + "and Hellinger distance >0.25. For proteomics data, an additional filter excluded rows "
        + "mapping to bovine proteins or entirely to reverse amino acid sequences. Gene lists were "
        + "pruned from the proteomic and mRNA experiments to keep only those genes observed in their "
        + "respective experimental proteomics, mRNA, or combined datasets. This pruning was completed "
        + "to account for the immune bias of the mRNA targeted panel. Pathway enrichment of "
        + "differentially expressed genes was performed by applying Fisher’s exact test to the "
        + "Molecular Signatures Database (MSigDB) gene lists. Prior to pathway enrichment of "
        + "differentially expressed gene lists, the MSigDB gene lists were filtered to keep only "
        + "those genes observed in their respective experimental proteomics, mRNA, or combined datasets. "
        + "This prefiltering was performed to account for the immune-related bias of the targeted-mRNA "
        + "panel. Literature interaction networks of differentially expressed genes were generated with "
        + "MetaCore (Clarivate Analytics, Philadelphia, PA, USA).",
        project_tags=tuple(),
        keywords=(
            "Immune checkpoint blockade",
            "Melanoma",
            "Label free expression proteomics",
        ),
        doi="10.6019/PXD033340",
        submission_type="COMPLETE",
        submission_date=datetime(year=2022, month=4, day=21),
        publication_date=datetime(year=2022, month=4, day=21),
        submitters=(
            PridePerson(
                title="Dr",
                affiliation="Moffitt Cancer Center",
                email="john.koomen@moffitt.org",
                country="United States",
                orcid="0000-0002-3818-1762",
                name="John Koomen",
                id="5300100",
            ),
        ),
        lab_pis=(
            PridePerson(
                title="Dr",
                affiliation="Cutaneous Oncology and Immunology Moffitt Cancer Center Tampa, FL, USA",
                email="joseph.markowitz@moffitt.org",
                country="",
                orcid="",
                name="Joseph Markowitz, MD/PhD",
                id="2383161",
            ),
        ),
        affiliations=(
            "Cutaneous Oncology and Immunology Moffitt Cancer Center Tampa, FL, USA",
            "Moffitt Cancer Center",
        ),
        instruments=(
            PrideParam(
                param_type="CvParam",
                cv_label="MS",
                accession="MS:1001911",
                name="Q Exactive",
                value=None,
            ),
        ),
        softwares=tuple(),
        quantification_methods=(
            PrideParam(
                param_type="CvParam",
                cv_label="PRIDE",
                accession="PRIDE:0000323",
                name="TIC",
                value=None,
            ),
        ),
        countries=tuple(),
        sample_attributes=tuple(),
        organisms=(
            PrideParam(
                param_type="CvParam",
                cv_label="NEWT",
                accession="9606",
                name="Homo sapiens (human)",
                value=None,
            ),
        ),
        organism_parts=tuple(),
        diseases=(
            PrideParam(
                param_type="CvParam",
                cv_label="DOID",
                accession="DOID:1909",
                name="Melanoma",
                value=None,
            ),
        ),
        references=(
            {
                "referenceLine": "Garg SK, Welsh EA, Fang B, Hernandez YI, Rose T, Gray J, Koomen "
                + "JM, Berglund A, Mulé JJ, Markowitz J. Multi-Omics and Informatics Analysis of "
                + "FFPE Tissues Derived from Melanoma Patients with Long/Short Responses to Anti-PD1 "
                + "Therapy Reveals Pathways of Response. Cancers (Basel). 2020 12(12)",
                "doi": "10.3390/cancers12123515",
                "pubmedId": 33255891,
                "id": 33255891,
            },
        ),
        identified_ptm_strings=(
            PrideParam(
                param_type="CvParam",
                cv_label="MOD",
                accession="MOD:00425",
                name="monohydroxylated residue",
                value=None,
            ),
            PrideParam(
                param_type="CvParam",
                cv_label="MOD",
                accession="MOD:00397",
                name="iodoacetamide derivatized residue",
                value=None,
            ),
        ),
        links=(
            PrideLink(
                rel="self",
                href="https://www.ebi.ac.uk/pride/ws/archive/v2/projects/PXD033340",
            ),
            PrideLink(
                rel="files",
                href="https://www.ebi.ac.uk/pride/ws/archive/v2/projects/PXD033340/files",
            ),
            PrideLink(
                rel="datasetFtpUrl",
                href="ftp://ftp.pride.ebi.ac.uk/pride/data/archive/2022/04/PXD033340",
            ),
        ),
    )


def test_project_parse_invalid() -> None:
    """Test the project parser for invalid input."""
    with pytest.raises(ValueError):
        PrideProjectSearchParser.parse([])


def test_project_parse(projects_data: Dict[str, Any]) -> None:
    """Test the project parser.

    Args:
    ----
        projects_data (Dict[str, Any]): PyTest data fixture
    """
    parsed = PrideProjectSearchParser.parse(projects_data)

    assert len(parsed) == 100
    assert parsed[0] == PrideProjectSearchResult(
        highlights={
            "projectDescription": (
                " as possibly carcinogen to <em>human</em>",
                " their carcinogenicity to <em>humans</em>",
            )
        },
        accession="PXD029842",
        title="Inhaled Multi-Walled Carbon Nanotubes modulate differently global gene and "
        + "protein expression in lung rats (additipon to PXD018900)",
        project_description="Multi-walled carbon nanotubes (MWCNTs) are among the most promising "
        + "nanomaterials because of their physical and chemical properties. However, since "
        + "they are biopersistent fiber-like materials which share similarities with asbestos, "
        + "concerns have arisen about their health effects. With their various industrial usages, "
        + "occupational exposure to MWCNTs may occur mainly by inhalation as these nanomaterials "
        + "can get aerosolised. The number of toxicological studies on CNTs has steadily increased "
        + "for the last decades. Different works showed that MWCNT exposure by inhalation or "
        + "intratracheal instillation could lead to pulmonary toxicity, such as lung inflammation, "
        + "genotoxicity, fibrosis or lung cancer (Kasai et al. 2015, Kasai et al. 2016, Porter "
        + "et al. 2013, Suzui et al. 2016). To date, only one MWCNT (MWNT-7) has been classified "
        + "as possibly carcinogen to human (Group 2B) while the others have not been as classifiable "
        + "as to their carcinogenicity to humans (Group 3) because of the lack of data on their "
        + "carcinogenic potential (IARC 2017). Because of the wide variety of CNTs with various "
        + "length, diameter or functionalisation, additional effort is required to assess "
        + "their pulmonary toxicity. As a complementary approach to conventional toxicological "
        + "assays, the omics methods are useful technologies for the mechanistic understanding "
        + "of the toxicological effects observed following exposure to chemicals and particulate "
        + "matters. Importantly, they can also be used as predictive tools for identifying "
        + "the mode of action of other particles with similar physical and chemical characteristics. "
        + "These molecular approaches may also be used for the discovery of exposure markers "
        + "or early markers of adverse effects, before the appearance of clinical signs of "
        + "a disease (Rahman et al. 2017). Several studies assessed gene expression alteration "
        + "following in vivo exposure to CNTs (Poulsen et al. 2015, Snyder-Talkington et al. "
        + "2013, Ellinger-Ziegelbauer and Pauluhn 2009). These omics approaches were used to "
        + "identify genes and pathways modulated in response to exposure. However, there are "
        + "still too few studies to assess the link between MWCNT physico-chemical properties, "
        + "global gene or protein expression profiles, and long-term effects. In a previous "
        + "study, we showed that inhalation of two pristine MWCNTs, the long and thick NM-401, "
        + "and the short and thin NM-403, induced alveolar neutrophilic granulocyte influx, "
        + "a hallmark of inflammation, which was proportional to the lung CNT BET surface "
        + "deposited dose (Gate et al. 2019). However, due to their different physical and "
        + "chemical properties, one could assume that these two CNTs may have diverse toxicological "
        + "profiles, but the conventional toxicology approaches used in this early work were "
        + "probably not sensitive enough to identify such differences. In order to gain additional "
        + "insight about their toxicological properties, in the current study, we compare the "
        + "alteration induced by the two MWCNTs on the transcriptome in the lung tissue and "
        + "the proteome in the broncho-alveolar lavage fluid (BALF) after rat exposure by "
        + "inhalation. The omics analyses were performed from 3 days up to 180 days.",
        sample_processing_protocol="Animal exposure Animal experiments were performed in "
        + "accordance with the EC Directive 2010/63/UE and with the approval of the local "
        + "Ethical Committee and the French Ministry for Research and Higher Education (APAFIS# "
        + "3468). The laboratory animal facility has been approved by The French Ministry of "
        + "Agriculture (Agreement #D54-547-10). The animal exposure has been previously published. "
        + "13-week-old female Sprague Dawley rats were housed in individually ventilated cages "
        + "(Tecniplast), maintained in 12h/12h light/dark cycles, and when not in restraining "
        + "tubes had ad libitum access to food and water. The experimental procedure is detailed "
        + "in (Gate et al. 2019). Animals were exposed to two concentrations of an aerosol of "
        + "CNTs (0.5 and 1.5 mg/m3) generated with an acoustic generator 2 x 3h/day, 5 days/week "
        + "for 4 weeks. Tissue sampling Lung tissues were collected 3, 30, 90 and 180 days after "
        + "the end of exposure following animal anesthesia with pentobarbital (60 mg/kg) and "
        + "exsanguination through the abdominal aorta. Accessory lung lobes were stored in "
        + "RNA later until use. Broncho-alveolar lavage (BAL) was performed on the left lung "
        + "as described in. Transcriptomic analysis from lung Accessory lung lobes from 6 "
        + "animals per group were disrupted and total RNAs were extracted and purified as "
        + "described previously. The RNA purity was analysed using Nanophotometer® spectrophotometer "
        + "(IMPLEN) and A260/A280 ratios were between 2 and 2.1. The RNA integrity was analysed"
        + " with the RNA Nano 6000 Assay kit on the Bioanalyzer 2100 system (Agilent Technologies) "
        + "and the RNA integrity numbers were above 6.2. The gene expression analyses were "
        + "assessed using Low Input Quick Amp Labelling kits (Agilent Technologies). Briefly, "
        + "100 ng of RNAs were reverse transcribed into cDNA and then cRNA labelled by Cyanine "
        + "3-CTP were synthesized and purified using RNeasy Mini Kit (Qiagen). The specific "
        + "activity was analysed for each sample (pmol Cyanine / µg RNA) and the threshold "
        + "was fixed to 6 as indicated by the manufacturer. Labelled cRNAs were hybridized "
        + "for 17h at 65°C onto Agilent G4853A SurePrint G3 Rat GE 8*60K v2 microarrays "
        + "(Agilent Technologies) and scanned on an Agilent G2505C microarray scanner with "
        + "a 3 µm resolution. Data were extracted with Agilent Feature Extraction software "
        + "version 11.0. Microarray data have been uploaded to the NCBI Gene Expression Omnibus"
        + " database under accession number GSE113532 (http://www.ncbi.nlm.nih.gov/geo/). "
        + "Proteomic analysis from broncho-alveolar lavage Filter-aided sample preparation "
        + "(FASP) was performed as previously described [Distiller 2016]. Briefly, 50 μg "
        + "protein was reduced by adding IM DTT to a final concentration of 0.1 M DTT. The "
        + "samples were mixed with 8 M urea in 0.1 M Tris-HCl, pH 8.9 (UA buffer) and loaded "
        + "onto ultracentrifugation units of nominal molecular weight cutoff 10.000 (Vivacon "
        + "500, catalogue no. VN01H02, Sartorius Stedim Biotech) and detergents were removed "
        + "washing the samples twice with the 8 M urea buffer. Afterward, proteins alkylated "
        + "with 0.05 M iodoacetamide in UA buffer (IAA). Afterward, the filter units were "
        + "washed three times with 50 mM ammonium bicarbonate (NH4HCO3). Proteins were digested "
        + "overnight at 37 °C using sequence grade trypsin (Trypsin Gold, Promega, Madison, WI) "
        + "at an enzyme-to-protein ratio of 1:50 (w/w). Peptides were recovered by centrifugation "
        + "and two additional washes with 50 mM NH4HCO3, combining the flow-throughs. Afterward, "
        + "samples were acidified with trifluoroacetic acid (TFA) to a final concentration "
        + "of 1% (v/v) TFA and lyophilized. 20 μg of the tryptic digests were loaded separately "
        + "and desalted on C18 Stage tip as described by (Rappsilber et al. 2007). After elution, "
        + "the samples were evaporated by placing in a CentriVap Concentrator with open caps "
        + "for approximately 10-15 minutes, until approximately 5 μL volume remained. The "
        + "sample was then resuspended in 15 μL of 0.1% TFA and stored at 4°C until mass "
        + "spectrometry analysis. MS Settings Each sample was run in triplicate on a Thermo "
        + "Scientific Q Exactive mass spectrometer connected to a Dionex Ultimate 3000 (RSLCnano) "
        + "chromatography system. Tryptic peptides were resuspended in 0.1% formic acid. Each "
        + "sample was loaded onto a fused silica emitter (75 μm ID), pulled using a laser puller "
        + "(Sutter Instruments P2000, Novato, CA, USA), packed with Reprocil Pur (Dr Maisch, "
        + "Ammerbuch-Entringen, Germany) C18 (1.9 μm; 12 cm in length) reverse phase media "
        + "and were separated by an increasing acetonitrile gradient over 60 min at a flow "
        + "rate of 250 nL/min direct into a Q-Exactive MS. The MS was operated in positive "
        + "ion mode with a capillary temperature of 320 °C, and with a potential of 2300 V "
        + "applied to the frit. All data was acquired while operating in automatic data dependent "
        + "switching mode. A high resolution (70,000) MS scan (300-1600 m/z) was performed "
        + "using the Q Exactive to select the 12 most intense ions prior to MS/MS analysis "
        + "using high-energy collision dissociation (HCD).",
        data_processing_protocol="Proteomic Analysis Proteins were identified and quantified "
        + "by MaxLFQ (Cox et al. 2014) by searching with the MaxQuant version 1.5 against "
        + "the Homo Sapiens reference proteome database (Uniprot). Modifications included "
        + "C carbamlylation (fixed) and M oxidation (variable). Gene and protein differential "
        + "expression analysis Raw microarray dataset was filtered in order to exclude unexpressed "
        + "probes and then normalized by the quantile method (Bolstad et al. 2003). Analysis of "
        + "differentially expressed genes (DEGs) was performed using a Bayesian model implemented "
        + "in the limma package in R/Bioconductor (Ritchie et al. 2015). The genes were considered "
        + "as significantly differentially expressed if (1) the expression changes were equal "
        + "to or larger than 1.5-fold in either direction for MWCNTs exposed group compared "
        + "to control group for each experimental conditions and (2) the BH-adjusted (FDR) "
        + "p-values were less than or equal to 0.05 (p ≤ 0.05). As input for proteomics analysis, "
        + "we used MaxQuant output files. The data were log2-transformed after removing reversed "
        + "matches and contaminants. Missing values in LFQ intensities were imputed using group mean "
        + "imputation with normal distribution correction as follows: when a protein was identified "
        + "in at least two replicas in a certain experimental condition, the missing values were "
        + "replaced by random draws from a Gaussian distribution centered on their mean value. "
        + "In all other cases, the missing values were replaced by random draws from a Gaussian "
        + "distribution centered in a minimal value for an experimental condition. Analysis of "
        + "differentially expressed proteins (DEPs) was performed using the limma package in "
        + "R/Bioconductor (Ritchie et al. 2015). The proteins were considered as significantly "
        + "differentially expressed if (1) the expression changes were equal to or larger than "
        + "1.3-fold in either direction for MWCNTs exposed group compared to control group "
        + "for each experimental conditions and (2) the BH-adjusted (FDR) p-values were less "
        + "than or equal to 0.1 (p ≤ 0.1). Unsupervised integration of transcriptomics and "
        + "proteomics datasets was performed using Multi-Omics Factor analysis (MOFA) (Argelaguet "
        + "et al. 2018). Pathway analysis of differentially expressed genes/proteins was performed "
        + "using the gprofiler2 package in R (Raudvere et al. 2019) and the KEGG database (Kanehisa "
        + "et al. 2017). Gene regulatory networks were inferred using GENIE3 algorithm, the "
        + "list of transcription factors was derived from the Animal TFDB 3.0 database (Hu "
        + "et al. 2019). Optimal threshold cut-off for selecting ranked gene pairs was identified "
        + "using a gene ontology semantic similarity approach described in (Zhernovkov et al. 2019). "
        + "Statistical analysis and data processing were performed with R version 3.6.1 "
        + "(https://www.r-project.org/) and RStudio version 1.1.383 (https://www.rstudio.com).",
        keywords=("Nanotubes mwcnts rat lung",),
        submission_date=datetime(year=2021, month=11, day=20),
        publication_date=datetime(year=2021, month=11, day=22),
        updated_date=datetime(year=2021, month=11, day=20),
        submitters=("David Matallanas",),
        lab_pis=("David Matallanas Gomez",),
        affiliations=("Systems Biology Ireland, University College Dublin",),
        instruments=("Q exactive",),
        organisms=("Rattus norvegicus (rat)",),
        organism_parts=(
            "Liver",
            "Epithelial cell of lung",
        ),
        references=(
            "Seidel C, Zhernovkov V, Cassidy H, Kholodenko B, Matallanas D, Cosnier F, Gaté "
            + "L. Inhaled multi-walled carbon nanotubes differently modulate global gene and "
            + "protein expression in rat lungs. Nanotoxicology. 2021 15(2):238-256",
        ),
        query_score=1.0,
        links=(
            PrideLink(
                rel="self",
                href="https://www.ebi.ac.uk/pride/ws/archive/v2/projects/PXD029842",
            ),
            PrideLink(
                rel="datasetFtpUrl",
                href="ftp://ftp.pride.ebi.ac.uk/pride/data/archive/2021/11/PXD029842",
            ),
        ),
    )


def test_pride_projects_parser_parse_simple(
    project_data: Dict[str, Any], project_data_parsed: PrideProject
) -> None:
    """Simple project parser test."""
    parsed = PrideProjectsParser.parse(project_data)
    assert parsed[0] == project_data_parsed


def test_pride_projects_parser_parse_sequence(
    project_data: Dict[str, Any], project_data_parsed: PrideProject
) -> None:
    """Sequenced project parser test."""
    parsed = PrideProjectsParser.parse([project_data])
    assert parsed[0] == project_data_parsed


def test_pride_projects_parser_parse_dict(
    project_data: Dict[str, Any], project_data_parsed: PrideProject
) -> None:
    """Dict project parser test."""
    parsed = PrideProjectsParser.parse({"projects": [project_data]})
    assert parsed[0] == project_data_parsed
