"""Tests for the pagination class."""

import pytest

from pride_api.pagination import PageInfo, Pagination
from pride_api.settings import Settings


@pytest.fixture(name="pager_default")
def pager_default_fixture() -> Pagination:
    """Fixture for a default pagination instance."""
    return Pagination(page_size=50)


@pytest.fixture(name="pager_custom")
def pager_custom_fixture() -> Pagination:
    """Fixture for a custom pagination instance."""
    return Pagination(
        page_size=50,
        current_page=2,
        page_info=PageInfo(size=50, total_elements=220, total_pages=5, number=3),
    )


def test_page_range() -> None:
    """Test if the page ranges are set up correctly."""
    with pytest.raises(ValueError):
        Pagination(page_size=Settings.pride_max_page_sizes[1] + 1)

    with pytest.raises(ValueError):
        pager = Pagination(page_size=50)
        pager.page_size = Settings.pride_max_page_sizes[1] + 1

    with pytest.raises(ValueError):
        Pagination(page_size=Settings.pride_max_page_sizes[0] - 1)

    # These should be fine
    Pagination(page_size=Settings.pride_max_page_sizes[0])
    Pagination(page_size=Settings.pride_max_page_sizes[0] + 1)
    Pagination(page_size=Settings.pride_max_page_sizes[1])
    Pagination(page_size=Settings.pride_max_page_sizes[1] - 1)


def test_next_page_custom(pager_custom: Pagination) -> None:
    """Check if a call to the next page increases the counter and the end is detected correctly."""
    assert pager_custom.next_page_idx() == 3
    assert pager_custom.has_next_page()

    assert pager_custom.next_page_idx() == 4
    assert not pager_custom.has_next_page()

    assert pager_custom.next_page_idx() == 4
    assert not pager_custom.has_next_page()


def test_last_page(pager_custom: Pagination, pager_default: Pagination) -> None:
    """Test if the last page is set correctly."""
    assert pager_custom.last_page_idx() == 4
    assert pager_default.last_page_idx() is None


def test_request_parameters(pager_custom: Pagination) -> None:
    """Test if the request parameters are correctly mapped."""
    expected = {
        "pageSize": 50,
        "page": 2,
    }
    assert pager_custom.request_parameters(auto_increment=False) == expected

    expected["page"] += 1
    assert pager_custom.request_parameters() == expected
