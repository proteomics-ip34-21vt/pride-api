"""Test the pride request."""
# pylint: disable=protected-access,unused-argument

import json
from typing import Any, Dict, List, Optional

import pytest
import requests
from pytest_mock import MockerFixture

from pride_api.exceptions import RequestUnsuccessfullException
from pride_api.pagination import PageInfo, Pagination
from pride_api.parsers.file import FileByProjectParser
from pride_api.parsers.pride_parser import PrideParser
from pride_api.pride_objects import PrideRawResponse
from pride_api.request import HrefLinks, PrideRequest
from pride_api.settings import Settings

paginator = Pagination(page_size=50)
ENDPOINT = "/testing"


@pytest.fixture(name="custom_request")
def custom_request_fixture() -> PrideRequest[Any]:
    """Shared PrideRequest instance."""
    return PrideRequest(
        endpoint=ENDPOINT,
        parameters={"param1": "value1", "param2": "value2"},
        parser=PrideParser,
        requires_pagination=True,
        pagination=paginator,
        headers={
            "custom1": "val1",
            "Accept": "application/vnd.api+json",
        },
    )


def test_init(custom_request: PrideRequest[Any]) -> None:
    """Tests the pride request mapping of input data."""
    assert custom_request.endpoint == ENDPOINT
    assert custom_request.parameters == {"param1": "value1", "param2": "value2"}
    assert custom_request.parser == PrideParser
    assert custom_request.pagination == paginator
    assert custom_request.headers == {
        "custom1": "val1",
        "Accept": "application/vnd.api+json",
    }

    # Check only the header & pagination
    req1: PrideRequest[Any] = PrideRequest(
        endpoint=ENDPOINT, headers={"custom1": "val1"}
    )
    assert req1.headers == {
        "custom1": "val1",
        "Accept": "application/json",
    }
    assert req1.pagination is not None
    assert req1.pagination.page_size == Settings.pride_default_page_size

    req2: PrideRequest[Any] = PrideRequest(
        endpoint="/testing",
        requires_pagination=False,
        retry_count=1,
    )
    assert req2.headers == {"Accept": "application/json"}
    assert req2.pagination is None


def request_dummy_response_success_paged(
    url: str, params: Dict[str, Any], headers: Dict[str, str]
) -> Optional[requests.Response]:
    """Requests a successfull paged source."""
    resp = requests.Response()
    resp.status_code = 200
    resp._content = json.dumps(
        {
            "page": {},
            "_links": {},
            "_embedded": [
                {"name": "project1"},
                {"name": "project2"},
            ],
        }
    ).encode()
    resp.encoding = "UTF8"
    return resp


def request_dummy_response_success_unpaged(
    url: str, params: Dict[str, Any], headers: Dict[str, str]
) -> Optional[requests.Response]:
    """Request a successfull unpaged source."""
    resp = requests.Response()
    resp.status_code = 200
    resp._content = json.dumps(
        [
            {"name": "project1"},
            {"name": "project2"},
        ]
    ).encode()
    resp.encoding = "UTF8"
    return resp


def request_dummy_response_exception(
    url: str, params: Dict[str, Any], headers: Dict[str, str]
) -> Optional[requests.Response]:
    """Tests if request exceptions are handeled correctly."""
    raise requests.RequestException()


def request_dummy_response_non_200(
    url: str, params: Dict[str, Any], headers: Dict[str, str]
) -> Optional[requests.Response]:
    """Test if non 200 responses are handeled correctly."""
    resp = requests.Response()
    resp.status_code = 500
    return resp


def test_next_page_success_paged(mocker: MockerFixture) -> None:
    """Test if next page call is handeled correctly when paged."""
    mocker.patch.object(
        PrideRequest,
        "_request_data",
        lambda _: {
            "pageSize": 30,
            "page": 1,
        },
    )
    mocker.patch.object(requests, "get", request_dummy_response_success_paged)
    mocker.patch.object(PrideRequest, "_process_page_info", lambda s, d: None)
    mocker.patch.object(PrideRequest, "_process_links", lambda s, d: None)

    spy = mocker.spy(PrideRequest, "_parse")

    req: PrideRequest[Any] = PrideRequest(endpoint="/testing")
    req._next_page()

    spy.assert_called_once_with(
        req,
        [
            {"name": "project1"},
            {"name": "project2"},
        ],
    )


def test_next_page_success_unpaged(mocker: MockerFixture) -> None:
    """Test if next page call is handeled correctly when unpaged."""
    mocker.patch.object(PrideRequest, "_request_data", lambda _: {})
    mocker.patch.object(requests, "get", request_dummy_response_success_unpaged)
    spy = mocker.spy(PrideRequest, "_parse")

    req: PrideRequest[Any] = PrideRequest(
        endpoint="/testing", requires_pagination=False
    )
    req._next_page()

    spy.assert_called_once_with(
        req,
        [
            {"name": "project1"},
            {"name": "project2"},
        ],
    )


def test_next_page_max_retries_exception(mocker: MockerFixture) -> None:
    """Test if next page call is handeled correctly when max retries is exceeded."""
    mocker.patch.object(
        PrideRequest,
        "_request_data",
        lambda _: {
            "pageSize": 30,
            "page": 1,
        },
    )
    mocker.patch.object(requests, "get", request_dummy_response_exception)

    req: PrideRequest[Any] = PrideRequest(endpoint="/testing")

    with pytest.raises(RequestUnsuccessfullException):
        req._next_page()


def test_next_page_max_retries_status_code(mocker: MockerFixture) -> None:
    """Test if next page call is handeled correctly when we have an invalid status code."""
    mocker.patch.object(
        PrideRequest,
        "_request_data",
        lambda _: {
            "pageSize": 30,
            "page": 1,
        },
    )
    mocker.patch.object(requests, "get", request_dummy_response_non_200)

    req: PrideRequest[Any] = PrideRequest(endpoint="/testing")

    with pytest.raises(RequestUnsuccessfullException):
        req._next_page()


def test_parse_parser(mocker: MockerFixture) -> None:
    """Test if we use the correct parser."""
    mocker.patch.object(FileByProjectParser, "parse")
    spy = mocker.spy(FileByProjectParser, "parse")

    req: PrideRequest[Any] = PrideRequest(
        endpoint="/testing", parser=FileByProjectParser
    )
    req._parse(
        [
            {
                "key1": "value1",
                "key2": "value2",
            }
        ]
    )

    spy.assert_called_once_with(
        [
            {
                "key1": "value1",
                "key2": "value2",
            }
        ]
    )


def test_parse_no_parser(mocker: MockerFixture) -> None:
    """Test if we have raw data returned when no parser is selected."""
    req: PrideRequest[Any] = PrideRequest(endpoint="/testing")
    raw: List[Any] = req._parse(
        [
            {
                "key1.1": "value1.1",
                "key1.2": "value1.2",
            },
            {
                "key2.1": "value2.1",
                "key2.2": "value2.2",
            },
        ]
    )

    assert raw == [
        PrideRawResponse(
            data={
                "key1.1": "value1.1",
                "key1.2": "value1.2",
            }
        ),
        PrideRawResponse(
            data={
                "key2.1": "value2.1",
                "key2.2": "value2.2",
            }
        ),
    ]


def test_request_data(mocker: MockerFixture) -> None:
    """Test the request data."""
    mocker.patch.object(
        Pagination,
        "request_parameters",
        lambda c: {
            "pageSize": 100,
            "page": 12,
        },
    )

    req: PrideRequest[Any] = PrideRequest(
        endpoint="/testing", parameters={"param1": "value1", "param2": "value2"}
    )
    assert req._request_data() == {
        "param1": "value1",
        "param2": "value2",
        "pageSize": 100,
        "page": 12,
    }


def test_request_data_mock_server(mocker: MockerFixture) -> None:
    """Test request data when using a mock server."""
    mocker.patch.object(
        Pagination,
        "request_parameters",
        lambda c: {
            "pageSize": 100,
            "page": 12,
        },
    )

    Settings.pride_is_mock_server = True
    req: PrideRequest[Any] = PrideRequest(
        endpoint="/testing", parameters={"param1": "value1", "param2": "value2"}
    )
    assert req._request_data() == {
        "param1": "value1",
        "param2": "value2",
        "pageSize": 100,
        "page": 12,
        "cache": str(Settings.pride_mock_cache_projects),
        "cache_files": str(Settings.pride_mock_cache_project_files),
    }
    Settings.pride_is_mock_server = False


def test_process_page_info() -> None:
    """Test if we handle the page info processing correctly."""
    pi = PrideRequest._process_page_info(
        {
            "size": 4358,
            "totalElements": 534121,
            "totalPages": 123,
            "number": 78,
        }
    )

    assert pi == PageInfo(
        size=4358,
        total_elements=534121,
        total_pages=123,
        number=78,
    )


def test_process_links() -> None:
    """Test if we handle the link info processing correctly."""
    links = PrideRequest._process_links(
        {
            "self": {
                "href": "self1",
            },
            "next": {
                "href": "next1",
            },
            "previous": {
                "href": "previous1",
            },
            "first": {
                "href": "first1",
            },
            "last": {
                "href": "last1",
            },
        }
    )

    assert links == HrefLinks(
        current="self1",
        next="next1",
        previous="previous1",
        first="first1",
        last="last1",
    )


def test_iterator(mocker: MockerFixture) -> None:
    """Test if we handle the iterator correctly."""
    mocker.patch.object(PrideRequest, "_next_page")
    spy = mocker.spy(PrideRequest, "_next_page")

    req: PrideRequest[Any] = PrideRequest(endpoint="/testing")
    req_iter = iter(req)
    next(req_iter)

    spy.assert_called_once()
