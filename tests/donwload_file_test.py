"""Downloading tests."""
# pylint: disable=duplicate-code,protected-access

import dataclasses
import tempfile
from datetime import datetime
from typing import Any
from urllib import error as urlliberror
from urllib import request

import pytest
from pytest_mock import MockerFixture

from pride_api.download_file import FTPDownloader
from pride_api.exceptions import RequestUnsuccessfullException
from pride_api.pride_objects import PrideFile, PrideParam


@pytest.fixture(name="pride_file")
def pride_file_fixture() -> PrideFile:
    """Sample pride file."""
    return PrideFile(
        project_accessions=("PXD005011",),
        accession="101877abd2d37c950afb8f4d9f97a1d79821193c2204fefc2e3c414377c5308f",
        file_category=PrideParam(
            param_type="CvParam",
            cv_label="PRIDE",
            accession="PRIDE:0000404",
            name="Associated raw file URI",
            value="RAW",
        ),
        checksum="7de555bc4f6fc0c2635e22981de31935ab0c95df",
        public_file_locations=(
            PrideParam(
                param_type="CvParam",
                cv_label="PRIDE",
                accession="PRIDE:0000468",
                name="Aspera Protocol",
                value="prd_ascp@fasp.ebi.ac.uk:pride/data/archive/2018/04/PXD005011/OTNCS25cm_GG01_C1-5.raw",
            ),
            PrideParam(
                param_type="CvParam",
                cv_label="PRIDE",
                accession="PRIDE:0000469",
                name="FTP Protocol",
                value="ftp://ftp.ebi.ac.uk/pride-archive/2018/04/PXD005011/OTNCS25cm_GG01_C1-5.raw",
            ),
        ),
        file_size_bytes=469601052,
        file_name="OTNCS25cm_GG01_C1-5.raw",
        compress=False,
        submission_date=datetime.now(),
        publication_date=datetime.now(),
        updated_date=datetime.now(),
        additional_attributes=tuple(),
        links=tuple(),
    )  # pylint: disable=duplicate-code


def test_download_ftp_file_by_string_ftp_to_https(mocker: MockerFixture) -> None:
    """Check if we update the ftp to a https link."""
    mocker.patch.object(request, "urlretrieve", lambda x, y: (y, None))
    spy = mocker.spy(request, "urlretrieve")

    with tempfile.TemporaryDirectory() as tmp:
        dl = FTPDownloader(tmp, exists_ok=True)
        path = dl._download_ftp_file_by_string(
            ftp_url="ftp://some-url.edu", name="testing"
        )

    spy.assert_called_once_with("https://some-url.edu", path)


def failing_urllib_request_mock(x: Any, y: Any) -> None:
    """Instantly fails the urllib request."""
    raise urlliberror.URLError("Testing!")


def test_download_ftp_file_by_string_exception(mocker: MockerFixture) -> None:
    """Test if we have the expected exceptions when downloading from a string."""
    mocker.patch.object(request, "urlretrieve", failing_urllib_request_mock)

    with tempfile.TemporaryDirectory() as tmp:
        dl = FTPDownloader(tmp, exists_ok=True)

        with pytest.raises(RequestUnsuccessfullException):
            dl._download_ftp_file_by_string(
                ftp_url="ftp://some-url.edu", name="testing"
            )


def test_download_ftp_file(mocker: MockerFixture, pride_file: PrideFile) -> None:
    """Test if we handle the ftp file downloading correctly."""
    mocker.patch.object(
        FTPDownloader, "_download_ftp_file_by_string", lambda s, url, path, retry: path
    )
    spy = mocker.spy(FTPDownloader, "_download_ftp_file_by_string")

    with tempfile.TemporaryDirectory() as tmp:
        dl = FTPDownloader(tmp, exists_ok=True)
    dl.download_ftp_file(pride_file=pride_file)

    spy.assert_called_once_with(
        dl,
        "ftp://ftp.ebi.ac.uk/pride-archive/2018/04/PXD005011/OTNCS25cm_GG01_C1-5.raw",
        "OTNCS25cm_GG01_C1-5.raw",
        FTPDownloader.RETRY,
    )


def test_download_ftp_file_no_ftp_location(
    mocker: MockerFixture, pride_file: PrideFile
) -> None:
    """Test if we have the eexpected exception when no ftp link is provided."""
    mocker.patch.object(
        FTPDownloader, "_download_ftp_file_by_string", lambda s, url, path, retry: path
    )

    pride_file_no_ftp = dataclasses.replace(
        pride_file, public_file_locations=(pride_file.public_file_locations[0],)
    )

    with tempfile.TemporaryDirectory() as tmp:
        dl = FTPDownloader(tmp, exists_ok=True)
    with pytest.raises(ValueError):
        dl.download_ftp_file(pride_file=pride_file_no_ftp)


def download_ftp_file_by_string_except(s: Any, url: Any, path: Any, retry: Any) -> None:
    """Instantly fails the download_by_file."""
    raise RequestUnsuccessfullException()


def test_download_ftp_file_excption(
    mocker: MockerFixture, pride_file: PrideFile
) -> None:
    """Check of the download_ftp_file exception is thrown."""
    mocker.patch.object(
        FTPDownloader,
        "_download_ftp_file_by_string",
        download_ftp_file_by_string_except,
    )

    with tempfile.TemporaryDirectory() as tmp:
        dl = FTPDownloader(tmp, exists_ok=True)
    dl.download_ftp_file(pride_file=pride_file)
    assert FTPDownloader.exception


def test_download_ftp_files(mocker: MockerFixture, pride_file: PrideFile) -> None:
    """Test of the download_ftp_files handles multiple PrideFiles."""
    mocker.patch.object(FTPDownloader, "download_ftp_file", lambda s, x: None)
    spy = mocker.spy(FTPDownloader, "download_ftp_file")

    with tempfile.TemporaryDirectory() as tmp:
        dl = FTPDownloader(tmp, exists_ok=True)
    dl.download_ftp_files(values=[pride_file, pride_file, pride_file])

    assert spy.call_count == 3
    spy.assert_called_with(dl, pride_file)


def test_download_ftp_files_threaded_worker_count(mocker: MockerFixture) -> None:
    """Test threaded worker count."""
    mocker.patch.object(FTPDownloader, "download_ftp_file", lambda s, x: None)

    with tempfile.TemporaryDirectory() as tmp:
        dl = FTPDownloader(tmp, exists_ok=True)

    with pytest.raises(ValueError):
        dl.download_ftp_files_threaded(pride_files=[], workers=0)

    with pytest.raises(ValueError):
        dl.download_ftp_files_threaded(pride_files=[], workers=6)

    dl.download_ftp_files_threaded(pride_files=[], workers=1)
    dl.download_ftp_files_threaded(pride_files=[], workers=5)


def test_download_ftp_files_threaded(
    mocker: MockerFixture, pride_file: PrideFile
) -> None:
    """Test of the threaded downloading handles multiple PrideFiles."""
    mocker.patch.object(FTPDownloader, "download_ftp_file", lambda s, x: None)
    spy = mocker.spy(FTPDownloader, "download_ftp_file")

    with tempfile.TemporaryDirectory() as tmp:
        dl = FTPDownloader(tmp, exists_ok=True)
    dl.download_ftp_files_threaded(
        pride_files=[pride_file, pride_file, pride_file, pride_file], workers=5
    )

    assert spy.call_count == 4
    spy.assert_called_with(dl, pride_file)
