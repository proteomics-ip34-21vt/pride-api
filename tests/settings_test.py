"""Settings test."""

from pride_api import defaults
from pride_api.settings import Settings


def test_settings() -> None:
    """Test that the default settings are correct."""
    assert_default_settings()


def assert_default_settings() -> None:
    """Test that the default settings are correct."""
    assert Settings.pride_sort_direction == defaults.PRIDE_SORT_DIRECTION
    assert Settings.pride_sort_conditions == defaults.PRIDE_SORT_CONDITIONS
    assert Settings.pride_default_page_size == defaults.PRIDE_DEFAULT_PAGE_SIZE
    assert Settings.pride_server == defaults.PRIDE_SERVER
    assert Settings.pride_max_page_sizes == defaults.PRIDE_MAX_PAGE_SIZES
    assert Settings.pride_default_retry_count == defaults.PRIDE_DEFAULT_RETRY_COUNT
    assert not Settings.pride_is_mock_server
    assert not Settings.pride_mock_cache_projects
    assert not Settings.pride_mock_cache_project_files


def test_reset_settings() -> None:
    """Test that the default settings are correct after resetting."""
    Settings.pride_sort_direction = "test"
    Settings.pride_sort_conditions = "test"
    Settings.pride_default_page_size = 10
    Settings.pride_server = "test"
    Settings.pride_max_page_sizes = (392, 2398)
    Settings.pride_default_retry_count = 99
    Settings.pride_is_mock_server = True
    Settings.pride_mock_cache_projects = True
    Settings.pride_mock_cache_project_files = True

    Settings.reset_to_defaults()
    assert_default_settings()
