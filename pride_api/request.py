"""Handles requests to the pride http api interface."""

from dataclasses import dataclass
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar

import requests

from pride_api import exceptions
from pride_api.pagination import PageInfo, Pagination
from pride_api.parsers.pride_parser import PrideParser, TPrideObjectBase
from pride_api.pride_objects import PrideObject, PrideRawResponse
from pride_api.settings import Settings


@dataclass
class HrefLinks:
    """Links returned by the pride DB api to link to the next, prev, .. pages of results."""

    current: str
    next: str
    previous: str
    first: str
    last: str


class PrideRequest(Generic[TPrideObjectBase]):
    """Used to make requests to the pride db api."""

    TPrideObjectBase = TypeVar("TPrideObjectBase", bound=PrideObject)

    def __init__(
        self,
        endpoint: str,
        parameters: Optional[Dict[str, Any]] = None,
        parser: Optional[Type[PrideParser[TPrideObjectBase]]] = None,
        requires_pagination: bool = True,
        pagination: Optional[Pagination] = None,
        headers: Optional[Dict[str, str]] = None,
        retry_count: int = Settings.pride_default_retry_count,
    ):
        """Initialize a new request iterator to the pride database.

        With each call to next(), the next page will be downloaded and parsed if a parser
        is available.

        Args:
            endpoint (str): API endpoint to be called. This does not include the server
                address
            parameters (Optional[Dict[str, Any]], optional): Optional parameters defined
                by the api endpoint. Defaults to None.
            parser (Optional[Type[PrideParser[TPrideObjectBase]]], optional): A
                PrideParser type, which can be used to parse the returned result into
                native python objects. If this parameter is not given, the parsed data
                will be returned as a dictionary or list.. Defaults to None.
            requires_pagination (bool, optional): If this endpoint requires pagination to
                be used. Defaults to True.
            pagination (Optional[Pagination], optional): A pagination object which is used
                to keep track of the current page. Use this to set a starting page.
                Defaults to None.
            headers (Optional[Dict[str, str]], optional): Any additional headers to be
                sent to the server. Defaults to None.
            retry_count (int, optional): How often a failed request should be retried
                before we call it a fail. Defaults to Settings.pride_default_retry_count.
        """
        self.endpoint: str = endpoint
        self.parser = parser

        if parameters is None:
            self.parameters = {}

        else:
            self.parameters = parameters

        self.pagination: Optional[Pagination] = None
        if requires_pagination:
            if pagination is None:
                pagination = Pagination(page_size=Settings.pride_default_page_size)

            self.pagination = pagination

        self.headers = {
            "Accept": "application/json",
        }
        if headers is not None:
            self.headers.update(headers)

        self.links: Optional[HrefLinks] = None
        self.retry_count = retry_count

    def __iter__(self) -> "PrideRequest[TPrideObjectBase]":
        """Return the iterator (us).

        Returns:
            PrideRequest[TPrideObjectBase]: Our selfs
        """
        return self

    def __next__(self) -> List[TPrideObjectBase]:
        """Get the next page from the pride database.

        Returns:
            List[TPrideObjectBase]: A list of parsed objects (If the parser is available)
        """
        return self._next_page()

    def _next_page(self) -> List[TPrideObjectBase]:
        """Get the next page of results from the server.

        Raises:
            exceptions.RequestUnsuccessfullException: Failed to fetch the request

        Returns:
            List[TPrideObjectBase]: If a parser is given, native
                Python objects of TPrideObjectBase will be returned. Otherwise the parsed
                json dict will be returned.
        """
        request_data = self._request_data()
        url = f"{Settings.pride_server}{self.endpoint}"

        request: requests.models.Response
        retry_count = max(1, self.retry_count)
        for retries_left in range(retry_count - 1, -1, -1):
            try:
                request = requests.get(
                    url,
                    params=request_data,
                    headers=self.headers,
                )

            except requests.RequestException as e:
                if retries_left == 0:
                    raise exceptions.RequestUnsuccessfullException() from e
                continue

            if request.status_code == 200:
                break

            if retries_left == 0:
                raise exceptions.RequestUnsuccessfullException(
                    f"Status code {request.status_code} was returned by the server!"
                )

        data = request.json()

        if self.pagination is not None:
            self.pagination.page_info = self._process_page_info(data["page"])
            self.links = self._process_links(data["_links"])
            return self._parse(data["_embedded"])

        return self._parse(data)

    def _parse(self, data: List[Dict[str, Any]]) -> List[TPrideObjectBase]:
        """Parse the given data with the set parser.

        If no parser is set, a Raw response is returned.

        Args:
            data (List[Dict[str, Any]]): The data that should be parsed

        Returns:
            List[TPrideObjectBase]: List of parsed data
        """
        if self.parser is None:
            return list(map(lambda x: PrideRawResponse(data=x), data))

        return self.parser.parse(data)

    def _request_data(self) -> Dict[str, Any]:
        data: Dict[str, str] = {
            **self.parameters,
        }

        if self.pagination is not None:
            data.update(self.pagination.request_parameters())

        if Settings.pride_is_mock_server:
            data["cache"] = str(Settings.pride_mock_cache_projects)
            data["cache_files"] = str(Settings.pride_mock_cache_project_files)

        return data

    @classmethod
    def _process_page_info(cls, data: Dict[str, int]) -> PageInfo:
        return PageInfo(
            size=data["size"],
            total_elements=data["totalElements"],
            total_pages=data["totalPages"],
            number=data["number"],
        )

    @classmethod
    def _process_links(cls, data: Dict[str, Dict[str, str]]) -> HrefLinks:
        return HrefLinks(
            current=data["self"]["href"],
            next=data["next"]["href"],
            previous=data["previous"]["href"],
            first=data["first"]["href"],
            last=data["last"]["href"],
        )
