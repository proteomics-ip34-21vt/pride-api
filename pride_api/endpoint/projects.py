"""PRIDE API project endpoints."""
from typing import Any, Dict, List, Optional, Sequence

from pride_api.defaults import PrideOrdering
from pride_api.pagination import Pagination
from pride_api.parsers.project import PrideProjectSearchParser, PrideProjectsParser
from pride_api.pride_objects import PrideProject, PrideProjectSearchResult
from pride_api.request import PrideRequest


def all_projects(pagination: Optional[Pagination] = None) -> PrideRequest[PrideProject]:
    """Return all the projects stored in the pride database.

    Args:
        pagination (Optional[Pagination], optional): Pagination instance to be used.
            Defaults to None.

    Returns:
        PrideRequest[PrideProject]: A request that resolves to a list of PrideProject
    """
    endpoint = "projects"

    return PrideRequest(
        endpoint=endpoint,
        parser=PrideProjectsParser,
        pagination=pagination,
    )


def project_search(
    keywords: Sequence[str],
    filters: Optional[Dict[str, Any]] = None,
    date_gap: str = "+1YEAR",
    sort_direction: PrideOrdering = PrideOrdering.ASCENDING,
    sort_fields: Optional[Sequence[str]] = None,
    pagination: Optional[Pagination] = None,
) -> PrideRequest[PrideProjectSearchResult]:
    """Search for projects in the pride database.

    Args:
        keywords (Sequence[str]): The sequence of keywords to be searched in the pride
            database. This may also be a project accession if we are only interested in a
            single project.
        filters (Optional[Dict[str, Any]], optional): A dictionary with direct field
            filters. These are the same fields as in `PrideProject`. Defaults to None.
        date_gap (str, optional): Undocumented on pride. Defaults to "+1YEAR".
        sort_direction (PrideOrdering, optional): Sort direction of the returned data.
            Defaults to PrideOrdering.ASCENDING.
        sort_fields (Optional[Sequence[str]], optional): Which fields should be sorted.
            These are the same fields as in `PrideProject`. Defaults to None.
        pagination (Optional[Pagination], optional): Pagination instance to be used.
            Defaults to None.

    Returns:
        PrideRequest[PrideProjectSearchResult]: A request that resolves to a list of
            PrideProjectSearchResult
    """
    sort_fields_com = ""
    if sort_fields is not None:
        sort_fields_com = ",".join(sort_fields)

    filters_list: List[str] = []
    if filters is not None:
        for k, v in filters.items():
            filters_list.append(f"{k}=={v}")

    endpoint = "search/projects"
    parameters = {
        "keyword": ",".join(keywords),
        "filter": ",".join(filters_list),
        "dateGap": date_gap,
        "sortDirection": sort_direction.value,
        "sortFields": sort_fields_com,
    }

    return PrideRequest(
        endpoint=endpoint,
        parameters=parameters,
        pagination=pagination,
        parser=PrideProjectSearchParser,
    )


def project_accession(accession: str) -> PrideRequest[PrideProject]:
    """Get a project from the pride database directly by it's accession.

    Args:
        accession (str): The accession used to identify the project

    Returns:
        PrideRequest[PrideProject]: A pride request to the project endpoint to fetch the
            project.
    """
    endpoint = f"projects/{accession}"

    return PrideRequest(
        endpoint=endpoint,
        parser=PrideProjectsParser,
        requires_pagination=False,
    )
