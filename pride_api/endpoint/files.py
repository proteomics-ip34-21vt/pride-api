"""PRIDE API files endpoints."""
from pride_api.parsers.file import FileByProjectParser
from pride_api.pride_objects import PrideFile
from pride_api.request import PrideRequest


def get_files_by_project(
    accession: str,
) -> PrideRequest[PrideFile]:
    """Get all Files in PRIDE Archive for the given accession.

    Args:
        accession (str): The project accession for which we would like to retrieve the
        files for.

    Returns:
        PrideRequest[PrideFile]: Returns a PrideRequest iterator
    """
    endpoint = "files/byProject"
    parameters = {"accession": accession}

    return PrideRequest(
        endpoint=endpoint,
        parameters=parameters,
        requires_pagination=False,
        parser=FileByProjectParser,
    )
