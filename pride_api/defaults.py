"""General defaults for the pride DB api."""

from enum import Enum
from typing import Final

PRIDE_SORT_DIRECTION: Final = "DESC"
PRIDE_SORT_CONDITIONS: Final = "submissionDate"
PRIDE_DEFAULT_PAGE_SIZE: Final = 50
PRIDE_SERVER: Final = "https://www.ebi.ac.uk/pride/ws/archive/v2/"
PRIDE_MAX_PAGE_SIZES: Final = (1, 100)
PRIDE_DEFAULT_RETRY_COUNT: Final = 3

DEFAULT_FILE_ENCODING: Final = "UTF8"


class PrideOrdering(Enum):
    """Defines the ordering methods available for the PRIDE db api."""

    ASCENDING = "ASC"
    DESCENDING = "DESC"
