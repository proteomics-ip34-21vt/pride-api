"""Collection of possible exceptions in the pride API."""


class PrideAPIException(Exception):
    """General pride api exception. This should be subclassed."""


class RequestUnsuccessfullException(PrideAPIException):
    """If a request to the pride DB was unsuccessfull."""


class RequestNotInitialized(PrideAPIException):
    """If the request was not initialized but we tried to get data anyway."""
