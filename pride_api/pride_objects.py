"""Collection of possible pride objects."""

from abc import ABC
from dataclasses import dataclass
from datetime import datetime
from typing import Any, Dict, Optional, Tuple, Union

from mashumaro.mixins.json import DataClassJSONMixin


class PrideObject(ABC, DataClassJSONMixin):  # type: ignore
    """A base class for all Pride objects to inherit from."""


@dataclass(frozen=True)
class PrideParam(PrideObject):
    """A pride parameter can contain a multitude of different values. For example, these are used on projects and files."""

    param_type: str
    cv_label: str
    accession: str
    name: str
    value: Optional[str]


@dataclass(frozen=True)
class PridePerson(PrideObject):
    """A single person on the pride db."""

    title: str
    affiliation: str
    email: str
    country: str
    orcid: str
    name: str
    id: str


@dataclass(frozen=True)
class PrideLink(PrideObject):
    """A link from the pride DB."""

    rel: str
    href: str


@dataclass(frozen=True)
class PrideRawResponse(PrideObject):
    """A wrapper class which simply contains the raw parsed data returned by the pride DB."""

    data: Dict[str, Any]


# pylint: disable=too-many-instance-attributes
@dataclass(frozen=True)
class PrideFile(PrideObject):
    """Represents a single pride Project file entry."""

    project_accessions: Tuple[str, ...]
    accession: str
    file_category: PrideParam
    checksum: str
    public_file_locations: Tuple[PrideParam, ...]
    file_size_bytes: int
    file_name: str
    compress: bool
    submission_date: datetime
    publication_date: datetime
    updated_date: datetime
    additional_attributes: Tuple[str, ...]
    links: Tuple[PrideLink, ...]


@dataclass(frozen=True)
class PrideProjectSearchResult(PrideObject):
    """Represents a single, reduced (search) pride Project."""

    highlights: Dict[str, Tuple[str, ...]]
    accession: str
    title: str
    project_description: str
    sample_processing_protocol: str
    data_processing_protocol: str
    keywords: Tuple[str, ...]
    submission_date: datetime
    publication_date: datetime
    updated_date: datetime
    submitters: Tuple[str, ...]
    lab_pis: Tuple[str, ...]
    affiliations: Tuple[str, ...]
    instruments: Tuple[str, ...]
    organisms: Tuple[str, ...]
    organism_parts: Optional[Tuple[str, ...]]
    references: Tuple[str, ...]
    query_score: float
    links: Tuple[PrideLink, ...]


@dataclass(frozen=True)
class PrideProject(PrideObject):
    """Represents a single, full pride Project."""

    accession: str
    title: str
    additional_attributes: Tuple[PrideParam, ...]
    project_description: str
    sample_processing_protocol: str
    data_processing_protocol: str
    project_tags: Tuple[str, ...]
    keywords: Tuple[str, ...]
    doi: str
    submission_type: str
    submission_date: datetime
    publication_date: datetime
    submitters: Tuple[PridePerson, ...]
    lab_pis: Tuple[PridePerson, ...]
    affiliations: Tuple[str, ...]
    instruments: Tuple[PrideParam, ...]
    softwares: Tuple[PrideParam, ...]
    quantification_methods: Tuple[PrideParam, ...]
    countries: Tuple[str, ...]
    sample_attributes: Tuple[str, ...]
    organisms: Tuple[PrideParam, ...]
    organism_parts: Tuple[PrideParam, ...]
    diseases: Tuple[PrideParam, ...]
    references: Tuple[Union[str, Dict[str, Any]], ...]
    identified_ptm_strings: Tuple[PrideParam, ...]
    links: Tuple[PrideLink, ...]
