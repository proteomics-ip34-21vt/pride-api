"""Collection of pride project parsers."""
from datetime import datetime
from typing import Any, Dict, List, Optional, Sequence, Tuple, Union

from pride_api.parsers.pride_parser import PrideParser
from pride_api.pride_objects import PrideLink, PrideProject, PrideProjectSearchResult


class PrideProjectSearchParser(PrideParser[PrideProjectSearchResult]):
    """Parses reduces search pride projects returned from the pride DB api."""

    @classmethod
    def parse(
        cls, data: Union[Dict[str, Any], Sequence[Dict[str, Any]]]
    ) -> List[PrideProjectSearchResult]:
        """Parse a reduced search pride project into a native python object.

        Args:
        ----
            data (Union[Dict[str, Any], Sequence[Dict[str, Any]]]): The input data
                received from the pride database

        Raises
        ------
            ValueError: If a list is provided instead of a dictionary

        Returns
        -------
            List[PrideProjectSearchResult]: The parsed pride Projects
        """
        if not isinstance(data, Dict):
            raise ValueError("Expected a Dict as an input")

        parsed: List[PrideProjectSearchResult] = []
        projects: List[Dict[str, Any]] = data["compactprojects"]

        for entry in projects:
            submission_date = datetime.strptime(
                entry["submissionDate"], PrideProjectSearchParser.PRIDE_DATE_FORMAT
            )
            publication_date = datetime.strptime(
                entry["publicationDate"], PrideProjectSearchParser.PRIDE_DATE_FORMAT
            )
            updated_date = datetime.strptime(
                entry["updatedDate"], PrideProjectSearchParser.PRIDE_DATE_FORMAT
            )

            links: List[PrideLink] = []
            for name, link_dict in entry["_links"].items():
                links.append(
                    PrideLink(
                        rel=name,
                        href=link_dict["href"],
                    )
                )

            organism_parts: Optional[Tuple[str, ...]] = None
            if (op := entry.get("organismParts")) is not None:
                organism_parts = tuple(op)

            highlights: Dict[str, Tuple[str, ...]] = entry["highlights"]
            highlights = {k: tuple(v) for k, v in highlights.items()}

            prj = PrideProjectSearchResult(
                highlights=highlights,
                accession=entry["accession"],
                title=entry["title"],
                project_description=entry["projectDescription"],
                sample_processing_protocol=entry["sampleProcessingProtocol"],
                data_processing_protocol=entry["dataProcessingProtocol"],
                keywords=tuple(entry["keywords"]),
                submission_date=submission_date,
                publication_date=publication_date,
                updated_date=updated_date,
                submitters=tuple(entry["submitters"]),
                lab_pis=tuple(entry.get("labPIs", [])),
                affiliations=tuple(entry["affiliations"]),
                instruments=tuple(entry["instruments"]),
                organisms=tuple(entry.get("organisms", [])),
                organism_parts=organism_parts,
                references=tuple(entry["references"]),
                query_score=entry["queryScore"],
                links=tuple(links),
            )

            parsed.append(prj)

        return parsed


class PrideProjectsParser(PrideParser[PrideProject]):
    """Parses full pride projects returned from the pride DB api."""

    @classmethod
    def parse(
        cls, data: Union[Dict[str, Any], Sequence[Dict[str, Any]]]
    ) -> List[PrideProject]:
        """Parse a full pride project into a native python object.

        Args:
        ----
            data (Union[Dict[str, Any], Sequence[Dict[str, Any]]]): The input data
                received from the pride database

        Returns
        -------
            List[PrideProject]: The parsed pride Projects
        """
        if isinstance(data, Sequence):
            return [cls.parse(x)[0] for x in data]

        if data.get("projects") is not None:
            return [cls.parse(x)[0] for x in data["projects"]]

        submission_date = datetime.strptime(
            data["submissionDate"], PrideProjectsParser.PRIDE_DATE_FORMAT
        )
        publication_date = datetime.strptime(
            data["publicationDate"], PrideProjectsParser.PRIDE_DATE_FORMAT
        )

        links: List[PrideLink] = []
        for name, link_dict in data["_links"].items():
            links.append(
                PrideLink(
                    rel=name,
                    href=link_dict["href"],
                )
            )

        prj = PrideProject(
            accession=data["accession"],
            title=data["title"],
            additional_attributes=cls._parse_pride_file_param(
                data["additionalAttributes"]
            ),
            project_description=data["projectDescription"],
            sample_processing_protocol=data["sampleProcessingProtocol"],
            data_processing_protocol=data["dataProcessingProtocol"],
            project_tags=tuple(data["projectTags"]),
            keywords=tuple(data["keywords"]),
            doi=data["doi"],
            submission_type=data["submissionType"],
            submission_date=submission_date,
            publication_date=publication_date,
            submitters=cls._parse_pride_person(data["submitters"]),
            lab_pis=cls._parse_pride_person(data["labPIs"]),
            affiliations=tuple(data["affiliations"]),
            instruments=cls._parse_pride_file_param(data["instruments"]),
            softwares=tuple(data["softwares"]),
            quantification_methods=cls._parse_pride_file_param(
                data["quantificationMethods"]
            ),
            countries=tuple(data["countries"]),
            sample_attributes=tuple(data["sampleAttributes"]),
            organisms=cls._parse_pride_file_param(data["organisms"]),
            organism_parts=cls._parse_pride_file_param(data["organismParts"]),
            diseases=cls._parse_pride_file_param(data["diseases"]),
            references=tuple(data["references"]),
            identified_ptm_strings=cls._parse_pride_file_param(
                data["identifiedPTMStrings"]
            ),
            links=tuple(links),
        )

        return [prj]
