"""Abstract implementation of the pride db api parser."""

from abc import ABCMeta, abstractmethod
from typing import Any, Dict, Final, Generic, List, Sequence, Tuple, TypeVar, Union

from pride_api.pride_objects import PrideLink, PrideObject, PrideParam, PridePerson

TPrideObjectBase = TypeVar("TPrideObjectBase", bound=PrideObject)


class PrideParser(Generic[TPrideObjectBase], metaclass=ABCMeta):
    """Parse a pride db api response into a native python object."""

    PRIDE_DATE_FORMAT: Final = "%Y-%m-%d"

    @classmethod
    @abstractmethod
    def parse(
        cls, data: Union[Dict[str, Any], Sequence[Dict[str, Any]]]
    ) -> List[TPrideObjectBase]:
        """Parse a pride db api response into a native python object.

        Args:
        ----
            data (Union[Dict[str, Any], Sequence[Dict[str, Any]]]): The input data
                received from the pride database

        Returns
        -------
            Tuple[TPrideObjectBase]: The parsed pride object
        """

    @classmethod
    def _parse_pride_file_param(
        cls, data: Sequence[Dict[str, Any]]
    ) -> Tuple[PrideParam, ...]:
        parsed: List[PrideParam] = []
        for entry in data:
            parsed.append(
                PrideParam(
                    param_type=entry["@type"],
                    cv_label=entry["cvLabel"],
                    accession=entry["accession"],
                    name=entry["name"],
                    value=entry.get("value"),
                )
            )

        return tuple(parsed)

    @classmethod
    def _parse_pride_person(
        cls, data: Sequence[Dict[str, Any]]
    ) -> Tuple[PridePerson, ...]:
        parsed: List[PridePerson] = []
        for entry in data:
            parsed.append(
                PridePerson(
                    title=entry["title"],
                    affiliation=entry["affiliation"],
                    email=entry["email"],
                    country=entry["country"],
                    orcid=entry["orcid"],
                    name=entry["name"],
                    id=entry["id"],
                )
            )

        return tuple(parsed)

    @classmethod
    def _parse_pride_links(
        cls, data: Sequence[Dict[str, Any]]
    ) -> Tuple[PrideLink, ...]:
        parsed: List[PrideLink] = []
        for entry in data:
            parsed.append(PrideLink(rel=entry["rel"], href=entry["href"]))

        return tuple(parsed)
