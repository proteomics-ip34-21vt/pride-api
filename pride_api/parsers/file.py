"""Collection of pride file parsers."""
from datetime import datetime
from typing import Any, Dict, List, Sequence, Union

from pride_api.parsers.pride_parser import PrideParser
from pride_api.pride_objects import PrideFile


class FileByProjectParser(PrideParser[PrideFile]):
    """Parses pride file entries returned from the pride DB api."""

    @classmethod
    def parse(
        cls, data: Union[Dict[str, Any], Sequence[Dict[str, Any]]]
    ) -> List[PrideFile]:
        """Parse a list of pride file entries into a native python object.

        Args:
            data (Union[Dict[str, Any], Sequence[Dict[str, Any]]]): The input data
                received from the pride database

        Raises:
            ValueError: If a dictionary is provided instead of a list

        Returns:
            List[PrideFile]: The parsed pride file entries
        """
        if not isinstance(data, list):
            raise ValueError("Expected to receive a list as the input")

        parsed: List[PrideFile] = []

        for entry in data:
            file_category = cls._parse_pride_file_param([entry["fileCategory"]])[0]
            public_file_locations = cls._parse_pride_file_param(
                entry["publicFileLocations"]
            )
            links = cls._parse_pride_links(entry["links"])

            submission_date = cls._from_timestamp_ms(entry["submissionDate"])
            publication_date = cls._from_timestamp_ms(entry["publicationDate"])
            updated_date = cls._from_timestamp_ms(entry["updatedDate"])

            file = PrideFile(
                project_accessions=tuple(entry["projectAccessions"]),
                accession=entry["accession"],
                file_category=file_category,
                checksum=entry["checksum"],
                public_file_locations=tuple(public_file_locations),
                file_size_bytes=entry["fileSizeBytes"],
                file_name=entry["fileName"],
                compress=entry["compress"],
                submission_date=submission_date,
                publication_date=publication_date,
                updated_date=updated_date,
                additional_attributes=tuple(entry["additionalAttributes"]),
                links=tuple(links),
            )

            parsed.append(file)

        return parsed

    @classmethod
    def _from_timestamp_ms(cls, timestamp: int) -> datetime:
        return datetime.utcfromtimestamp(round(timestamp / 1000))
