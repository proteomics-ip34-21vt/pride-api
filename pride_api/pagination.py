"""Collection of helpers for the pagination of pride DB requests."""

from dataclasses import dataclass
from typing import Any, Dict, Optional

from pride_api.settings import Settings


@dataclass
class PageInfo:
    """Information about the pride page returned by the pride DB api call."""

    size: int
    total_elements: int
    total_pages: int
    number: int


class Pagination:
    """Help with paginating multi page endpoint results."""

    def __init__(
        self,
        page_size: int,
        current_page: int = -1,
        page_info: Optional[PageInfo] = None,
    ) -> None:
        """Help with paginating multi page endpoint results.

        Keeps track of the current page, total page count and how many items should be
        fetched.

        Args:
            page_size (int): The count of items to be fetched with each request.
            current_page (int, optional): On which page are currently located. With each
                next() call this will be increased.
                If one would like to get the first page, we need a value of -1, if we would
                like to have page #5, we set this to 4. Defaults to -1.
            page_info (Optional[PageInfo], optional): Page information returned by the
                latest server response. This should not be set from the outside. Defaults
                to None.
        """
        self.page_size = page_size
        self.current_page = current_page
        self.page_info = page_info

    @property
    def page_size(self) -> int:
        """Size of the page / how many items should be retrieved at max per request.

        Returns:
            int: Max item count to be retrieved per request
        """
        return self.__page_size

    @page_size.setter
    def page_size(self, new: int) -> None:
        mi, ma = Settings.pride_max_page_sizes
        if not mi <= new <= ma:
            raise ValueError("Invalid range")

        self.__page_size = new

    def next_page_idx(self) -> int:
        """Get the next page idx.

        Returns:
            int: The next page index. If we have reached the end, it returns the page idx of
                the last page
        """
        if self.has_next_page():
            self.current_page += 1

        return self.current_page

    def has_next_page(self) -> bool:
        """Check if we have a next page and have not reached the end.

        Returns:
            bool: If we still have pages left
        """
        return (
            self.page_info is None or self.current_page < self.page_info.total_pages - 1
        )

    def last_page_idx(self) -> Optional[int]:
        """Get the last page index.

        Returns:
            Optional[int]: If we currently don't have any page information, this will
            return None, else it returns the last available page index.
        """
        if self.page_info is not None:
            return self.page_info.total_pages - 1
        return None

    def request_parameters(self, auto_increment: bool = True) -> Dict[str, Any]:
        """Retrieve the request parameters for the pagination for a api request.

        Args:
            auto_increment (bool, optional):  If the current page idx should automatically
                be incremented for the request. Defaults to True.

        Returns:
            Dict[str, Any]: The request parameters related to the pagination
        """
        page = self.next_page_idx() if auto_increment else self.current_page
        return {
            "pageSize": self.page_size,
            "page": page,
        }
