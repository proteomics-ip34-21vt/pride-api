"""Collection of helpers to download a file from the pride database."""

import concurrent.futures
import logging
import os
from typing import List, Optional, Sequence
from urllib import error as urlliberror
from urllib import request

from pride_api.exceptions import RequestUnsuccessfullException
from pride_api.pride_objects import PrideFile

logger = logging.getLogger(__name__)


class FTPDownloader:
    """A downloader for ftp, files and dir will be removed, when this program is closed.

    Raises
    ------
        RequestUnsuccessfullException: risen, if the ftp_link is corrupted
    """

    RETRY: int = 3  # RETRY > 0
    exception: bool = False

    def __init__(self, output_path: str, exists_ok: bool = False) -> None:
        """Init method.

        Args:
        ----
            output_path (str): The full path where the downloaded files should be saved.
            exists_ok (bool): If it's okay, if the output path already exists (Mostly only for test purposes)
        """
        os.makedirs(output_path, exist_ok=exists_ok)
        self.output_path: str = output_path

    def _download_ftp_file_by_string(
        self, ftp_url: str, name: str, counter: int = RETRY
    ) -> Optional[str]:
        """Download a file, by given ftp url, saves it in self.directory.

        Args:
        ----
            ftp_url (str): The ftp url to download from
            name (str): set the name of the file to this, non-ascii chars will be removed
            counter (int):

        Returns
        -------
            Optional[str]: full path to the downloaded file, if successful, otherwise None
        """
        path = None
        name = name.encode("ascii", "ignore").decode()
        # pylint: disable=consider-using-with
        tmp_file = open(
            os.path.join(self.output_path, name),
            "w+b",
        )
        logger.info("Try downloading file: %s.", tmp_file.name)

        try:
            ftp_url = ftp_url.replace("ftp://", "https://", 1)
            path, _ = request.urlretrieve(ftp_url, tmp_file.name)  # nosec
            logger.info("Finished downloading file: %s.", tmp_file.name)
        except urlliberror.URLError as e:
            if counter > 0:
                path = self._download_ftp_file_by_string(ftp_url, name, counter - 1)
            else:
                logger.warning("failed to download file: %s", tmp_file.name)
                raise RequestUnsuccessfullException() from e

        finally:
            tmp_file.close()
        return path

    def download_ftp_file(self, pride_file: PrideFile) -> None:
        """Download file with ftp, sets PrideFile.local_path.

        Args:
        ----
            pride_file (PrideFile): Of which file the files should be downloaded.
        """
        param = list(
            filter(lambda x: x.name == "FTP Protocol", pride_file.public_file_locations)
        )
        if len(param) == 0 or param[0].value is None:
            raise ValueError("Could not find a valid ftp download link.")

        try:
            self._download_ftp_file_by_string(
                param[0].value, pride_file.file_name, FTPDownloader.RETRY
            )
        except RequestUnsuccessfullException:
            FTPDownloader.exception = True

    def download_ftp_files(self, values: Sequence[PrideFile]) -> None:
        """Download all files from given Sequence[PrideFile] using self.download_ftp_files.

        Args:
        ----
            values (Sequence[PrideFile]): Of which projects the files should be downloaded.
        """
        for pride_file in values:
            self.download_ftp_file(pride_file)

    def download_ftp_files_threaded(
        self, pride_files: List[PrideFile], workers: int = 5
    ) -> None:
        """Download all files form given List[PrideFile] multithreaded using self.download_ftp_files.

        Args:
        ----
            pride_files (List[PrideFile])
            workers (int, optional): max amount of downloads at the same time.
                                     Defaults to 5.

        Raises
        ------
            ValueError: is risen, if not 0 < workers <= 5
        """
        if not 0 < workers <= 5:
            raise ValueError("Choose between 1 to 5 workers")

        with concurrent.futures.ThreadPoolExecutor(max_workers=workers) as executor:
            executor.map(self.download_ftp_file, pride_files)
