"""Settings for pride requests."""
from typing import Tuple

from . import defaults


class Settings:
    """Settings to be used while making requests to the PRIDE api."""

    pride_sort_direction = defaults.PRIDE_SORT_DIRECTION
    pride_sort_conditions = defaults.PRIDE_SORT_CONDITIONS
    pride_default_page_size = defaults.PRIDE_DEFAULT_PAGE_SIZE
    pride_server = defaults.PRIDE_SERVER
    pride_max_page_sizes: Tuple[int, int] = defaults.PRIDE_MAX_PAGE_SIZES
    pride_default_retry_count = defaults.PRIDE_DEFAULT_RETRY_COUNT
    pride_is_mock_server = False
    pride_mock_cache_projects = False
    pride_mock_cache_project_files = False

    @classmethod
    def reset_to_defaults(cls) -> None:
        """Reset the settings to the default values."""
        cls.pride_sort_direction = defaults.PRIDE_SORT_DIRECTION
        cls.pride_sort_conditions = defaults.PRIDE_SORT_CONDITIONS
        cls.pride_default_page_size = defaults.PRIDE_DEFAULT_PAGE_SIZE
        cls.pride_server = defaults.PRIDE_SERVER
        cls.pride_max_page_sizes = defaults.PRIDE_MAX_PAGE_SIZES
        cls.pride_default_retry_count = defaults.PRIDE_DEFAULT_RETRY_COUNT
        cls.pride_is_mock_server = False
        cls.pride_mock_cache_projects = False
        cls.pride_mock_cache_project_files = False
