"""A sample implementation on how to get al the unique quantification methods in the pride database."""
import logging
import sys
import time
from dataclasses import dataclass
from typing import Dict, List

from pride_api.defaults import DEFAULT_FILE_ENCODING
from pride_api.endpoint.projects import all_projects
from pride_api.exceptions import PrideAPIException
from pride_api.pagination import Pagination
from pride_api.pride_objects import PrideParam, PrideProject

logger = logging.getLogger("UNIQUE_QUANTIFICATION")
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
logger.addHandler(handler)


@dataclass
class ModsQuant:
    """Wrapper class with all mods and quantifications."""

    mods: List[PrideParam]
    quantifications: List[PrideParam]


projects = all_projects(pagination=Pagination(page_size=50))
seen_workflows: List[str] = []
lines: Dict[str, List[str]] = {}  # k: quantification, v: List of projects
identified_mods_phos: Dict[str, ModsQuant] = {}  # k: project accession
with_quant = 0  # pylint: disable=invalid-name
without_quant = 0  # pylint: disable=invalid-name


if projects.pagination is None:
    logger.error("Pagination is not enabled")
    sys.exit()

try:
    while projects.pagination.has_next_page():
        try:
            prjs: List[PrideProject] = next(projects)
            logger.info(
                "Page: %s / %s",
                projects.pagination.current_page,
                projects.pagination.last_page_idx(),
            )

        except PrideAPIException:
            logger.info("Failed to fetch page!")
            continue

        for prj in prjs:
            workflows = [x.name for x in prj.quantification_methods]

            if len(workflows) > 0:
                for meth in prj.quantification_methods:
                    if meth.name in lines:
                        lines[meth.name].append(prj.accession)
                    else:
                        lines[meth.name] = [prj.accession]

            if len(workflows):
                with_quant += 1
            else:
                without_quant += 1

            mods = list(
                filter(
                    lambda x: x.cv_label == "MOD" and "phospho" in x.name.lower(),
                    prj.identified_ptm_strings,
                )
            )

            if len(mods) > 0:
                identified_mods_phos[prj.accession] = ModsQuant(
                    mods=mods, quantifications=list(prj.quantification_methods)
                )

        time.sleep(0.5)

except Exception as e:  # pylint: disable=broad-except
    logger.exception("Failed to process data", exc_info=e)


def save_quantifications(items: Dict[str, List[str]]) -> None:
    """Save the quantifications to the "quantifications.tsv" file.

    Args:
    ----
        items (Dict[str, List[str]]): Dict of which the key identifies the quantification,
                                        the list all the projects tagged wit the quantfication.
    """
    str_lines: List[str] = []
    # Generate the lines
    for k, i in items.items():
        str_lines.append(f"{k}\t{len(i)}\t{','.join(i)}")

    with open("quantifications.tsv", "a+", encoding=DEFAULT_FILE_ENCODING) as fh:
        fh.write("\n".join(str_lines) + "\n")


def save_mods(items: Dict[str, ModsQuant]) -> None:
    """Save the applied modifcations to the "mods.tsv"file.

    Args:
    ----
        items (Dict[str, ModsQuant]): The items to be checked for modifications.
    """
    str_lines: List[str] = []
    # Generate the lines
    for k, i in items.items():
        found_mods = [x.name for x in i.mods]
        quantifications = list(map(lambda x: x.name, i.quantifications))
        str_lines.append(f"{k}\t{','.join(found_mods)}\t{','.join(quantifications)}")

    with open("mods.tsv", "a+", encoding=DEFAULT_FILE_ENCODING) as fh:
        fh.write("\n".join(str_lines) + "\n")


save_quantifications(lines)
save_mods(identified_mods_phos)

logger.info("Stats: %s | %s", with_quant, without_quant)
